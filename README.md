# kevin

Repository to hold information relevant to Kevin Yuexin Chen's ANU internship work from 2021 Semester 2.

## Contents

### Documents and Information from ANU
* [contact_details](.docs/from_anu/contact_details.md)
* [feedback](.docs/from_anu/feedback.md)
* [ANU Placement information](.docs/from_anu/placement.md)
* [poster_and_pitch](.docs/from_anu/poster_and_pitch.md)
* [ANU High Level Schedule](.docs/from_anu/schedule.md)
* [ANU Description of Statement of Work](.docs/from_anu/statement_of_work.md)
* [tutorials_and_mentoring](.docs/from_anu/tutorials_and_mentoring.md)
* [work_portfolio](.docs/from_anu/work_portfolio.md)

### Documents to be provided to ANU
* [Week 6 Feedback Notes](.docs/for_anu/feedback/feedback_1.md)
* [Week 10 Feedback Notes](.docs/for_anu/feedback/feedback_2.md)
* [Pitch](.docs/for_anu/poster_and_pitch/pitch.md)
* [Poster](.docs/for_anu/poster_and_pitch/poster.md)
* [Project Constraints](.docs/for_anu/statement_of_work/constraints.md)
* [Project Costs](.docs/for_anu/statement_of_work/costs.md)
* [Project Deliverables](.docs/for_anu/statement_of_work/deliverables.md)
* [Expectations](.docs/for_anu/statement_of_work/expectations.md)
* [Host's Vision and Objectives](.docs/for_anu/statement_of_work/hosts_vision_and_objectives.md)
* [Key Stakeholders](.docs/for_anu/statement_of_work/key_stakeholders.md)
* [Project Milestones](.docs/for_anu/statement_of_work/project_milestones.md)
* [Resources](.docs/for_anu/statement_of_work/resources.md)
* [Risks](.docs/for_anu/statement_of_work/risks.md)
* [Schedule](.docs/for_anu/statement_of_work/schedule.md)
* [Setup](.docs/for_anu/statement_of_work/setup.md)
* [Value Addition](.docs/for_anu/statement_of_work/value_addition.md)
* [Cover Letter](.docs/for_anu/work_portfolio/cover_letter.md)
* [Curriculum Vitae](.docs/for_anu/work_portfolio/curriculum_vitae.md)
* [Portfolio Piece](.docs/for_anu/work_portfolio/portfolio_piece.md)

### Research
This directory is for holding information and notes from research activity.

### src
This directory is for holding all source code.

### test
This directory is for holding all tests to be run against source code.

### data
This directory is for holding all data that may be used by the source code. For example training, test, or validation data.
Ideally the contents of this directory should not be directly committed to the repository but the application should refer to the data from this location.
If the data cannot be downloaded via script, please add it to the [civilise.ai data repository](https://gitlab.com/civilise-ai/data) and link from there.
