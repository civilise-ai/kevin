# Host's Vision and Objectives

### Background

With the continuous development of big data technology, artificial 
intelligence has gradually been used in all areas of life. Farming, 
as the foundation of food production, some companies are using machine 
learning or artificial intelligence to optimize the efficiency of agriculture.  
My client - Civilise.ai, aims to make sustainable and regenerative 
Australian farming easy and accessible, applying artificial intelligence to 
Geospatial data. They currently focused on using artificial intelligence to 
analyze the farming system and land resources based on satellite data and 
computer version algorithm.

### Project Goal

**Reinforcement Learning(RL)** is an area of machine learning(ML) concerned 
with how intelligent agents ought to take actions in an environment in order to 
maximize the notion of cumulative reward[1]. RL does not depend on historical 
data, but it can adapt to many environmental changes. In a non-stationary environment, 
it can find the ‘best’ action to maximize reward. My client wants me to research the 
RL method and code some general examples or libraries for future use. 