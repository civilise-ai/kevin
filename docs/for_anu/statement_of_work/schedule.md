# Detailed schedule

...

| Week | Day | Date   | Planned Hours | Actual Hours | Notes |
|------|-----|--------|---------------|--------------|-------|
| W01  | Fri | Jul 30 | 8 | 8  |   |
| W01  | Sat | Jul 31 | 7 | 7  |   |
| W02  | Fri | Aug 06 | 8 | 8  |   |
| W02  | Sat | Aug 07 | 7 | 7  |   |
| W03  | Fri | Aug 13 | 8 | 8  |   |
| W03  | Sat | Aug 14 | 7 | 7 |   |
| W04  | Fri | Aug 20 | 8 |   |   |
| W04  | Sat | Aug 21 | 7 |   |   |
| W05  | Fri | Aug 27 | 8 |   |   |
| W05  | Sat | Aug 28 | 7 |   |   |
| W06  | Fri | Sep 03 | 8 |   |   |
| W06  | Sat | Sep 04 | 7 |   |   |
| W07  | Fri | Sep 24 | 8 |   |   |
| W07  | Sat | Sep 25 | 7 |   |   |
| W08  | Fri | Oct 01 | 8 |   |   |
| W08  | Sat | Oct 01 | 7 |   |   |
| W09  | Fri | Oct 08 | 8 |   |   |
| W09  | Sat | Oct 09 | 7 |   |   |
| W10  | Fri | Oct 15 | 8 |   |   |
| W10  | Sat | Oct 15 | 7 |   |   |
| W11  | Fri | Oct 22 | 8 |   |   |
| W11  | Sat | Oct 23 | 7 |   |   |
| W12  | Fri | Oct 29 | 8 |   |   | 
| W12  | Sat | Oct 30 | 7 |   |   | 
