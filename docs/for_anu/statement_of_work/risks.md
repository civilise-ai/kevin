# Risks

### Technical issue

The project includes some difficulties in implementing reinforcement learning models and the environments set up.

### Accuracy uncertainty

For different reinforcement learning model is not guaranteed to perform precisely same as information recorded.

### Communication issue

Due to the Covid-19 and remote learning, there is an unavoidable delay in the transmission and feedback of information.