# Costs

In this project, except for the potential cost of some non-open source data sets, most of the other expense on the project is the time cost for implementation.

## Who will bear costs

Most of the cost can be ignored.