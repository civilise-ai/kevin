# Host and Other Stakeholder Expectations
> Since this project is research-based, the main requirements
> from the client side are defined as follows:

1. Research on Reinforcement Learning Model.
2. Implementation of Reinforcement Learning Model.
3. Performance on real-world problems.
