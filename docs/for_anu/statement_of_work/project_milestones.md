# Project Milestones

No|Milestones|Estimated Delivery
:------:|:----------|:-----------------
1|Background Research|Aug 27
2|Implementation of Reinforcement Learning model|Sep 10
3|Training the data and reviewing the model|Sep 24
4|Assign some model to specific tasks|Oct 15
5|Record relevant data and make analysis/report|Oct 29