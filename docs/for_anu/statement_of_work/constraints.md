# Constraints

## Technical Constraints

### Internship Contract

The project is related to the ANU’s Internship program, so the employee’s behavior and Intellectual Property agreement must comply with the Internship Contract provided by ANU.

### IEEE Standard

The coding part for this project should follow the IEEE standard which makes this project modifiable and iterative in the future.

## Other Constraints

### Covid-19

Currently, all parts of the world have been affected by Covid-19, so the project is limited in communication and transmission.
