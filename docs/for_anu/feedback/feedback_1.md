# Feedback 1
Week 6

## Outputs -- How valuable are the student's outputs to key stakeholders, given level of effort and other resources available?

...

## Decision Making -- How are the student processes?
### Decision Making
### Decision Implementation
### Decision Evaluation
### Decision Reflection
...

## Teamwork -- How is the student working together with the host organisation to achieve project outcomes?
...

## Communication -- How is the student communicating with, and managing the expectations of key stakeholders?
...

## Reflection -- How is the student reviewing feedback and acting on it to improve their performance?
...
