# Work Portfolio
The Work Portfolio is an application for a position of any kind, built primarily around the student's experience in their Internship.  It includes:
* Brief CV (2 pages) and cover letter addressing selection criteria
* Portfolio piece presenting the project.
