# Tutorials and Mentoring
Students participate in weekly tutorials, starting in week 3.  The tutorials primarily support the students in understanding and completing their assessment tasks.

Students participate in a mentoring program that includes an introductory work 1-on-1 mentoring and small group mentoring circles through the semester.  This program supports the students in understanding, adapting to, and growing with their professional roles and challenges.  **Prior to the 1-on-1 mentoring sessions, the mentors will check in with the supervisors to see if there are any particular issues of concern** that the students might benefit from focused support in addressing.
