# ANU Computing Internship Schedule - Semester 2 2021

| Start  | End    | Week    | Activities                               | Assessment            |
|--------|--------|---------|------------------------------------------|-----------------------|
| Jul 19 | Jul 23 | Week  0 | **ANU/Host meetings**                    |                       |
| Jul 26 | Jul 30 | Week  1 | **Placement begins**, Mentoring Workshop |                       |
| Aug 02 | Aug 06 | Week  2 |                                          |                       |
| Aug 09 | Aug 13 | Week  3 | 1-on-1 Mentoring (*supervisor input)     |                       |
| Aug 16 | Aug 20 | Week  4 | Mentoring Circle                         | **Statement of Work** |
| Aug 23 | Aug 27 | Week  5 |                                          |                       |
| Aug 30 | Sep 03 | Week  6 |                                          | **Feedback 1**        |
| Sep 06 | Sep 10 | Break 1 | *No placement*                           |                       |
| Sep 13 | Sep 17 | Break 2 | *No placement*                           |                       |
| Sep 20 | Sep 24 | Week  7 | 1-on-1 Mentoring (*supervisor input)     | Reflection 1          |
| Sep 27 | Oct 01 | Week  8 |                                          |                       |
| Oct 04 | Oct 08 | Week  9 | Mentoring Circle                         | Poster & Pitch        |
| Oct 11 | Oct 15 | Week 10 |                                          | **Feedback 2**        |
| Oct 18 | Oct 22 | Week 11 |                                          |                       |
| Oct 25 | Oct 29 | Week 12 | **Placement ends**                       | Reflection 2          |
| Nov 01 | Nov 05 |   Exams |                                          | Work Portfolio        |

Information sourced from email from Penny Kyburz, ANU to john.forbes@civilise.ai on 2021 July 5th.
