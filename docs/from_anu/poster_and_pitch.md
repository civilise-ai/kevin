# Poster and Pitch
Students produce a poster and a short video that showcases their work.
* Posters/pitches might be presented at TechLauncher Showcase.
* **Please advise if there are any concerns with student project materials being public facing**.
