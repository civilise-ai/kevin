# Statement of Work
From ANU documents:

The aim of the Statement of Work is to demonstrate that students are prepared from the coming semester and should include:

* articulation of host's vision and objectives
* the key stakeholders, what do they do, and how they interact
* host and other stakeholder expectations
* how the project will make things better for the host and other stakeholders
* project milestones, scheduling and deliverables for the semester
* technical and other constraints (e.g., reliability security safety)
* identification of resources, risks, potential costs and who will bear them
* The **Statement of Work**should be signed/initialed by the host supervisor
* The setup of tooling for development, management of tasks, and project repository.
