# Feedback
Each student and their supervisor will submit a feedback report via an online form, using the following Feedback criteria:
* Outputs: how valuable are the student's outputs to key stakeholders, given the level of effort and other resources available to them?
* Decision Making: how are the student's processes for making, implementing, evaluating, and learning from decisions?
* Teamwork: how is the student working together with the host organisation to achieve project outcomes?
* Communication: how is the student communicating with, and managing the expectations of, key stakeholders?
* Reflection: how is the student reviewing feedback and acting on it to improve their performance?

**Supervisors will be invited to complete the feedback form in Weeks 6 and 10.**  Timely completion of the form is appreciated and necessary for students to reflect on the feedback for the reflection assessment items.
