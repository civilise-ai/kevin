# Placement
From ANU documents:
> Students undertake their placement for 15 hours per week.
> This 15 hour is spent working onsite at the host organisation or the remote working equivalent.
> During these 15 hours, regular access to, and support from, the host superivsor should be available.
> Students do not work in their placements during the teaching break, unless they need to catch up on hours missed.
> Mentoring, tutorials, and assessment activities are in addition to the 15 hours.

Information sourced from email from Penny Kyburz, ANU to john.forbes@civilise.ai on 2021 July 5th.

