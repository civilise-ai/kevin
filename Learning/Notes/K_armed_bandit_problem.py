# Book: Reinforcement Learning - second edition, Richard S. Sutton and Andrew G. Barto (2018)
#
# Reference: https://blog.csdn.net/JerryZhang__/article/details/83959887?utm_medium=distribute.pc_relevant.none-task-blog-2%7Edefault%7EBlogCommendFromMachineLearnPai2%7Edefault-2.control&depth_1-utm_source=distribute.pc_relevant.none-task-blog-2%7Edefault%7EBlogCommendFromMachineLearnPai2%7Edefault-2.control
# Chapter 2 - 2.7
# Multi-armed Bandit Problem
#
# 1. MAB(Multi-armed Bandit), a reinforcement learning model. Assume you have k-armed bandit, each time insert one coin, and
# it will return coins back with some probability. Goal: to maximize the rewards.
#
# 2. Exploration & Exploitation
# Exploration: check each mean reward, assert same probability with each bandit.
# Exploitation: choose the maximum average rewards bandit.
# Exploration cannot guarantee the optimal choose, Exploitation cannot guarantee potential better choose.
# This called the Exploration-Exploitation dilemma.
#
# 3. Epsilon greedy and softmax algorithm
# Epsilon greedy: with the probability of epsilon, choose that action, and 1 - epsilon as exploitation,
# Softmax: each time take the action, calculate the average reward with softmax algorithm, and use this value to choose the bandit.


import numpy as np
import random
import matplotlib.pyplot as plt

def init():
    tau = 0.1
    epsilon = 0.01
    T = 3000
    reward = [1,1]
    probability = [0.4, 0.2]

    count = dict.fromkeys(['1','2'], 0)
    Q = dict.fromkeys(['1', '2'], 0)
    sum_p = sum([np.exp(i/tau) for i in Q.values()])
    P = [np.exp(i/tau)/sum_p for i in Q.values()]
    return tau, epsilon, T, reward, probability, count, Q, P

# epsilon- Greedy Epsilon
# use epsilon probability to explore o.w. exploit
# epsilon = 0, no explore only greedy on one action -> converge to 1 fix point
# epsilon = 0.01, 99.1% explore -> converge and rewards up
# epsilon = 0.1, much faster in early time, but may not converge after 3000 steps,
tau,epsilon,T,reword,probability,count,Q,P =init()
r=0
r_all=list()
#Q={"1":0.4,'2':0.2} - action value
for i in range(T):
    if random.uniform(0,1)<epsilon:
        k=random.randint(1,2)
    else:
        k=int(max(Q,key=Q.get))
#    print("此时选择的老虎机为第{}个".format(k))
    v=reword[k-1] if probability[k-1]>random.uniform(0,1) else 0
#    print(v)
    r=(r*(i)+v)/(i+1)
    r_all.append(r)
    count[str(k)] +=1
    Q[str(k)]=Q[str(k)]+(v-Q[str(k)])/count[str(k)]
#    print("平均奖赏为:",r)
plt.plot(list(range(T)),r_all,color='r')



#Softmax算法
r=0
r_all=list()
tau,epsilon,T,reword,probability,count,Q,P =init()
for i in range(T):
    sum_p=sum([np.exp(i/tau) for i in Q.values()])
    P=[np.exp(i/tau)/sum_p for i in Q.values()]
    k=int(np.random.choice(list(Q.keys()),p=P))
    v=reword[k-1] if probability[k-1]>random.uniform(0,1) else 0
    r=(r*(i)+v)/(i+1)
    r_all.append(r)
    count[str(k)] +=1
    Q[str(k)]=Q[str(k)]+(v-Q[str(k)])/count[str(k)]
#    print("平均奖赏为:",r)
plt.plot(list(range(T)),np.array(r_all),color='b')
plt.show()

# code reference: https://blog.csdn.net/bingfeiqiji/article/details/99694773?utm_medium=distribute.pc_relevant.none-task-blog-2%7Edefault%7EBlogCommendFromBaidu%7Edefault-5.control&depth_1-utm_source=distribute.pc_relevant.none-task-blog-2%7Edefault%7EBlogCommendFromBaidu%7Edefault-5.control
