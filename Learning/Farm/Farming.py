#!/usr/bin/env python
# coding: utf-8

# Get the input file data from the weather.csv and transfer that data into array format.

# In[39]:


import pandas as pd
import numpy as np
import matplotlib.pyplot as plt

df = pd.read_csv('weatherAUS.csv')
data_collection = np.array(df)
print(data_collection)
print(len(data_collection))
# data = data.reshape(1, len(data)).tolist()
# print(data)

# data = df.set_index('Date').T.to_dict('list')
# data


# In[50]:


class Plant():
    def __init__(self, id):
        self.id = id
        # threshold: water maximum per day
        # period: the reward return time
        # water: the water required o.w. fail to plant -> reward = -200
        if id == 1:
            self.name = 'Plant 1'
            self.threshold = 100
            self.period = 14
            self.reward = 4
            self.water = 0.0
            self.curr = 1
            self.water_collect = 0.0
        elif id == 2:
            self.name = 'Plant 2'
            self.threshold = 70
            self.period = 21
            self.reward = 1.2
            self.water = 5
            self.curr = 1
            self.water_collect = 0.0
        elif id == 3:
            self.name = 'Plant 3'
            self.threshold = 50
            self.period = 25
            self.reward = 2
            self.water = 10
            self.curr = 1
            self.water_collect = 0.0
        elif id == 4:
            self.name = 'Plant 4'
            self.threshold = 40
            self.period = 30
            self.reward = 12
            self.water = 100
            self.curr = 1
            self.water_collect = 0.0


# In[51]:


# the starting date ---------------------------------------------------------------------
start_date = '2009-01-01'

location = 'Albury'

data = []


for i in range(len(data_collection)):
    d = data_collection[i]
    if d[0] == start_date and d[1] == location:
        index = i
        break

        
if index == None:
    print("start date or location is invalid.")
else:
    # check for 2000 days ---------------------------------------------------------------------
    data = data_collection[index:index + 2000]

print(data)
    


# In[95]:


def argmax(q_values):
    
    top_value = float("-inf")
    ties = []
    
    for i in range(len(q_values)):
        # find the maximum q values
        if q_values[i] > top_value:
            top_value = q_values[i]
            ties = [i]
        elif q_values[i] == top_value:
            ties.append(i)
    return np.random.choice(ties)

def EpsilonGreedyAgent(q_values, epsilon):
    r = np.random.random()
    if r < epsilon:
        current_action = np.random.randint(0, len(q_values))
    else:
        current_action = argmax(q_values)
    return current_action
    

def plantMature(q_values, a_record, last_action, reward):
    a_record[last_action] += 1
    q_values[last_action] = q_values[last_action] + 1 / a_record[last_action]     * (reward - q_values[last_action])
    
def checkMature(farm, q_values, a_record):
    farmings = farm.farmings
    # print("Check Mature or not")
    found = True
    while found:
        found = False
        for t in range(len(farmings)):
            
            x = farmings[t]
            if x.curr == x.period:
                
                found = True
                if x.water_collect < x.water:
                    # give the nagetive reward if water not satisfied-------------
                    plantMature(q_values, a_record, x.id - 1, -200)
                    farm.reward -= 200
                # update q values
                else:
                    plantMature(q_values, a_record, x.id - 1, x.reward * x.water_collect)
                    farm.reward += x.reward * x.water_collect
                farmings.pop(t)
                break
                
def check_end_of_day(farm, rainfall, q_values, a_record):
    farmings = farm.farmings
    tp = 3
    index = 0
    while index < tp:
        f = farmings[index]
        found = False
        if rainfall > f.threshold:
            # update q values -------------------   -2000
            plantMature(q_values, a_record, f.id - 1, -2000)
            farm.reward -= 2000
            farmings.pop(index)
            found = True
        else:
            f.curr += 1
            f.water_collect += rainfall
        
        if found:
            tp -= 1
        else:
            index += 1
        
    # check whether action should make
    while(len(farmings) < 3):
        r = EpsilonGreedyAgent(q_values, e)
        print("The agent select the plant " + str(r + 1))
        farmings.append(Plant(r + 1))
        


# In[99]:


# background set up

# farming lands - 3 area
# farmings = []

class Farmings():
    def __init__(self, l = []):
        self.farmings = []
        if l != []:
            for i in l:
                self.farmings.append(Plant(i))
        self.reward = 0.0
                
farm = Farmings([2, 3, 2])
farmings = farm.farmings

# q values to record the action
# q[0] for plant 1
# q[1] for plant 2
# q[2] for plant 3
# q[3] for plant 4
# use Epsilon Greedy Agent to update the action
q_values = [0, 0, 0, 0]
# action 0 for grow plant 1
# action 1 for grow plant 2
# action 2 for grow plant 3
# action 3 for grow plant 4
# action recorded
a_record = [0, 0, 0, 0]
e = 0.3



# check for each day
for i in range(len(data)):
    print("-----------------------------" + str(i) + "--------------------------")
    print(data[i][0])
    rainfall = data[i][4]
    # print(type(rainfall))
    if str(rainfall) == 'nan':
        rainfall = 0.0
    print(rainfall)
    
    # debugger --
#     if i == 30:
#         break
        
    # check whether the crop is mature
    checkMature(farm, q_values, a_record)
    
    # check whether action should make
    while(len(farmings) < 3):
        r = EpsilonGreedyAgent(q_values, e)
        print("The agent select the plant " + str(r + 1))
        farmings.append(Plant(r + 1))
        
    # end day check water
    check_end_of_day(farm, rainfall, q_values, a_record)
    
    
#     for j in farmings:
#         print (j.__dict__)
        
    print("q values: ")
    print(q_values)
    print("Reward: " + str(farm.reward))
        
    


# In[ ]:





# In[ ]:




