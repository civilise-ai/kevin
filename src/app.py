class App:
  def __init__(self):
    print('App object created')

  def run(self):
    print('application running')

  def __del__(self):
    print('App object destroyed')
  