This directory is for holding all data that may be used by the source code. For example training, test, or validation data.
Ideally the contents of this directory should not be directly committed to the repository but the application should refer to the data from this location.
If the data cannot be downloaded via script, please add it to the [civilise.ai data repository](https://gitlab.com/civilise-ai/data) and link from there.
